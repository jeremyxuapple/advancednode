const mongoose = require('mongoose');

const exec = mongoose.Query.prototype. exec;

mongoose.Query.prototype.exec = function() {
  // have to use fucntion instead of arrow function, to avoid 'this' keyword
  // being messedup

  console.log('runing query');

  return exec.apply(this, arguments);
}
